# -*- coding: utf-8 -*-

import cPickle
import shutil
import os
import ibnd_creator
import hyc_creator
import pressure_creator
import subprocess as sb
import numpy as np
import sys

# avoid source folder being cluttered with compiled files of source
sys.dont_write_bytecode = True

Transport = True    # True activates transport simulation, false deactivates it.
Speed = 'normal'    # Choose between 'normal' and 'slow' for 15 or 5 cm/s
homogeneous = True  # don't read hydraulic conductivity from file but set equally in dat-file

path = dict()
path['destDir']= '/work/laubeg/flumeIsrael/hom_1_5/'  # Output folder
path['filesDir']=path['destDir']+'files/'
path['templateDir']=path['filesDir']+'Templates/'
path['currentJobDir']=''
path['streamtracers_file'] = path['filesDir']+'streamtracers.py'

# Model parameters
# The number of cells and their size both need to be changed in this dictionary if a finer mesh is wished. 

hom_aniso = np.array([0.019992, 0.025950, 0.006595])
hom_aniso_fit = 1.863 * hom_aniso
hom_iso = AdjustEB * np.array([1., 1., 1.])*np.exp(np.average(np.log([4.47E-02, 2.69E-02, 1.20E-03]), weights=[4297.5, 3817.5, 2910]))
hom_iso_fit = 1.513 * hom_iso

# weights include coarse layer. Whithout coarse layer: [2985, 3817.5, 2910]
# hom_1.863xK_aniso_normal: 1.863 * np.array([0.019992, 0.025950, 0.006595])
# hom_1.513xK_normal: 1.513 * AdjustEB * np.array([1., 1., 1.])*np.exp(np.average(np.log([4.47E-02, 2.69E-02, 1.20E-03]), weights=[4297.5, 3817.5, 2910]))
# hom_aniso_normal: np.array([0.019992, 0.025950, 0.006595])
# hom_geomMean_normal: AdjustEB * np.array([1., 1., 1.])*np.exp(np.average(np.log([4.47E-02, 2.69E-02, 1.20E-03]), weights=[4297.5, 3817.5, 2910]))

parameters = {
    # the cell sizes should always be fractions of 0.005
    'sedimentlength' : 5.447, # Flume length / m
    'cellx'         : 0.005,  # Size of the cells in x
    'sizey'         : 0.145,  # Size of the model/Flume in y
    'sizeinter'     : 0.005,  # size of the homogeneous layer beneath the ripples
    'nr_cells_y'    : 5,      # Number of cells in y
    'hetcellz'      : 0.005,  # Size of the heterogeneous cells in z
    'botcellz'      : 0.0125, # size of the bottom layer cells in z
    'ripplerise'    : 0.15,   # length of a ripple rise
    'ripplefall'    : 0.05,   # length of a ripple fall
    'rippleheight'  : 0.02,   # height of the ripples
    'waterDepth'    : 0.07,   # water depth at crest
    'nr_cells_bot'  : 2,      # Number of cells in the bottom layer
    'nr_cells_mid'  : 34,     # Number of cells in the heterogeneous mid
    'nr_cells_inter': 1,      # Number of cells in the homogeneous layer beneath the ripples
    'nr_cells_top'  : 5,      # Number of cells in the ripple layer 
    'numeric_xoffset': 0.04,  # 4 cm offset in x to match the ripples with the heterogeneity
    'k0'            : 4.47E-02 , # coarse Sand   # Raw values need to be adjusted to match flume data, will be done later.
    'k1'            : 2.69E-02 , # medium Sand   # 
    'k2'            : 1.20E-03,  # fine Sand
    'homogeneousK'  : hom_iso_fit*1.5
}

# bot size should be 0.025, nr cells = 2, with 0.0125 size
# het size should be 0.17, number of cells = 34 , with 0.005 size
# inter size is 0.005  , number of cells = 1, with 0.005 size
# ripple size is 0.02, number of cells = 5, with 0.005 size
# resulting in a final height of 0.22, with 42 cells in z direction

# GW-Boundary values, each represents a case, + == Inflow(Gaining conditions), - == Outflow (losing conditions)
if Speed == 'normal':
    influx = np.array([
                    0.0, 
                    -10.504, 
                    -25.,
                    -39.5714, 
                    -59.2806, 
                    -97.9735, 
                    -146.0626, 
                    -197.0928, 
                    -254.0, 
                    -291.8196,
                    10.,
                    25.,
                    39.5714, 
                    59.2806, 
                    97.9735, 
                    150.0, 
                    190.9814, 
                    245.6, 
                    284.0], np.float) / (100*24*60*60)
# if Speed == 'normal':
    # influx = np.array([-197.0928], np.float) / (100*24*60*60)
    
elif Speed == 'slow':
    influx = np.array([
                    0., 
                    -10., 
                    -20., 
                    -29.55, 
                    -50., 
                    -80., 
                    10., 
                    20., 
                    30., 
                    49., 
                    78.], np.float)  / (100*24*60*60)     
        
        
trans = {
    # 'finaltime' : 2.88E4,                # Maximum simulated time
    'finaltime' : 1.0E-5,                # Maximum simulated time
    'maxtimestep' : 1.0E4,               # Largest time step 
    'mintimestep' : 1.0E-6,              # Smallest time step
    # 'output' : (600, 2400, 3600, 28800),  # output time steps in s, the amount of times has to be a multiple of 4, 0s is written by default
    'output' : (1.0E-6, 3.3E-6, 6.7E-6, 1.0E-5),
    # 'output' : (240, 480, 720, 960, 
                # 1200, 1440, 1680, 1920, 
                # 2160, 2400, 2640, 2880, 
                # 3120, 3360, 3600, 3840, 
                # 4080, 4320, 4560, 4800, 
                # 5040, 5280, 5520, 5760, 
                # 6000, 6240, 6480, 6720, 
                # 6960, 7200, 7440, 7680, 
                # 7920, 8160, 8400, 8640, 
                # 8880, 9120, 9360, 9600, 
                # 9840, 10080, 10320, 10560,
                # 10800, 11040, 11280, 11520,
                # 11760, 12000, 12240, 12480),

    # 'output' : (60, 120, 180, 240,
                # 300, 360, 420, 480,
                # 540, 600, 660, 720,
                # 780, 840, 900, 960,
                # 1020, 1080, 1140, 1200,
                # 1260, 1320, 1380, 1440,
                # 1500, 1560, 1620, 1680,
                # 1740, 1800, 1860, 1920,
                # 1980, 2040, 2100, 2160,
                # 2220, 2280, 2340, 2400,
                # 2460, 2520, 2580, 2640,
                # 2700, 2760, 2820, 2880,
                # 2940, 3000, 3060, 3120,
                # 3180, 3240, 3300, 3360,
                # 3420, 3480, 3540, 3600,
                # 3660, 3720, 3780, 3840,
                # 3900, 3960, 4020, 4080,
                # 4140, 4200, 4260, 4320,
                # 4380, 4440, 4500, 4560,
                # 4620, 4680, 4740, 4800,
                # 4860, 4920, 4980, 5040,
                # 5100, 5160, 5220, 5280,
                # 5340, 5400, 5460, 5520,
                # 5580, 5640, 5700, 5760,
                # 5820, 5880, 5940, 6000,
                # 6060, 6120, 6180, 6240,
                # 6300, 6360, 6420, 6480,
                # 6540, 6600, 6660, 6720,
                # 6780, 6840, 6900, 6960,
                # 7020, 7080, 7140, 7200,
                # 7260, 7320, 7380, 7440,
                # 7500, 7560, 7620, 7680,
                # 7740, 7800, 7860, 7920,
                # 7980, 8040, 8100, 8160,
                # 8220, 8280, 8340, 8400,
                # 8460, 8520, 8580, 8640,
                # 8700, 8760, 8820, 8880,
                # 8940, 9000, 9060, 9120,
                # 9180, 9240, 9300, 9360,
                # 9420, 9480, 9540, 9600,
                # 9660, 9720, 9780, 9840,
                # 9900, 9960, 10020, 10080,
                # 10140, 10200, 10260, 10320,
                # 10380, 10440, 10500, 10560,
                # 10620, 10680, 10740, 10800),
    'activation' : Transport,
    'concentration' : 1.0                # Topside boundary condition in mol/l
}

# Compute the Size of Cells in y-Direction
parameters['celly'] = parameters['sizey'] / parameters['nr_cells_y']
# Compute the size of the cells in the homogeneous layer beneath the ripples
parameters['cellzi'] = parameters['sizeinter'] / parameters['nr_cells_inter']
# X-offset in number of cells
parameters['xoffset'] = int(parameters['numeric_xoffset']/parameters['cellx'])

# Dictionary for the Elliott & Brooks Boundary Condition
pressure = {
    'speed' : Speed,        # normal for 15 m/s or slow for 5 m/s 
    'read'  :   Pressure,         # Read pressure from file, if false it uses Elliott & Brooks
    'periods':  parameters['sedimentlength']/(parameters['ripplerise']+parameters['ripplefall']),      # Number of periods
    'phaseshift': (parameters['numeric_xoffset'] - 0.025)/0.20*360.,  # Phase shift for the sine in deg, with this the maximum is at the mid of the ripple rise 
    'slope':        0.00,#086,    # overall slope of the flume in m/m # roughly 4(?????) mm total slope, from the converted paraview export data 
                               # with ( p_rgh_mean difference * 0.0001019977334 )/ sedimentlength  
                               # 0.00086 for normal                           
}
# Set stream velocity.
if pressure['speed']=='normal':
    pressure['streamVelocity'] = 0.15    
    pressure['empConstant'] = 0.044
elif pressure['speed']=='slow':
    pressure['streamVelocity'] = 0.05
    pressure['empConstant'] = 0.094

# Delete old results if they exist.
if os.path.exists(path['destDir']):
    print "Removing old results...."
    shutil.rmtree(path['destDir'])
    print "Done.\n"

# Create new working directory and copy databases.
shutil.copytree('.',path['filesDir'])
# shutil.copyfile('streamtracers.py',path['streamtracers_file'])
# shutil.copyfile('flume.py',path['filesDir']+'flume.py') 

# Save parameters for the other files to use.
with open(path['filesDir']+'input.pickle0', 'w') as f:
    cPickle.dump([parameters, pressure], f)

# Copy dat-file template. 
shutil.copyfile(path['templateDir']+'template.dat', path['filesDir']+'min3p.dat')
# Open the new dat-file and change it.
with open(path['filesDir']+'min3p.dat', 'r') as f:
    min3pDatLines = f.readlines()
    
    # Spatial discretization.
    indexes = [ i for i, string in enumerate(min3pDatLines) if string.startswith("'spatial discretization'") ]
    min3pDatLines[indexes[0] + 2] = '%i                 ; number of control volumes in x\n'%(int(parameters['sedimentlength']/parameters['cellx']))
    min3pDatLines[indexes[0] + 3] = '0 %f                 ; xmin;xmax\n'%(float(int(parameters['sedimentlength']*100))/100)
    min3pDatLines[indexes[0] + 5] = '%i                 ; number of control volumes in y\n'%(int(parameters['nr_cells_y']))
    min3pDatLines[indexes[0] + 6] = '0 %f                 ; ymin;ymax\n'%(parameters['sizey'])
    min3pDatLines[indexes[0] + 8] = '%i                 ; number of control volumes in z\n'%(parameters['nr_cells_top'] + parameters['nr_cells_mid'] + parameters['nr_cells_bot'] + parameters['nr_cells_inter'])
    min3pDatLines[indexes[0] + 9] = '0 %f                 ; zmin;zmax\n'%0.22
    
    if homogeneous:
        indexes = [ i for i, string in enumerate(min3pDatLines) if string.startswith("'read hydraulic conductivity field from file'") ]
        min3pDatLines[indexes[0]] = "!'read hydraulic conductivity field from file' \n"
        indexes = [ i for i, string in enumerate(min3pDatLines) if string.startswith("'hydraulic conductivity in x-direction'") ]
        min3pDatLines[indexes[0] + 1] = '%f              ; K_xx \n'%parameters['homogeneousK'][0]
        indexes = [ i for i, string in enumerate(min3pDatLines) if string.startswith("'hydraulic conductivity in y-direction'") ]
        min3pDatLines[indexes[0] + 1] = '%f              ; K_yy \n'%parameters['homogeneousK'][1]
        indexes = [ i for i, string in enumerate(min3pDatLines) if string.startswith("'hydraulic conductivity in z-direction'") ]
        min3pDatLines[indexes[0] + 1] = '%f              ; K_zz \n'%parameters['homogeneousK'][2]

    # Set database directory.
    indexes = [ i for i, string in enumerate(min3pDatLines) if string.startswith("'database directory'") ]
    min3pDatLines[indexes[0] + 1] = "'%sdatabases' \n" %(path['filesDir'])
    
    # Transport active or not ?.
    if trans['activation']:
        # Transport Activation
        indexes = [ i for i, string in enumerate(min3pDatLines) if string.startswith("'global control parameters'") ]
        min3pDatLines[indexes[0] + 5] = '.true.              ;reactive_transport\n'
        # Times for spatial output.
        indexes = [ i for i, string in enumerate(min3pDatLines) if string.startswith("'output of spatial data'") ]
        min3pDatLines[indexes[0] + 1] = '%i                 ;number of output times (spatial data)\n'%(int(len(trans['output'])))
        for i in range(int(len(trans['output'])/4)):
            min3pDatLines[indexes[0] + 2 + i] = '%f %f %f %f \n'%(trans['output'][0+i*4],trans['output'][1+i*4],trans['output'][2+i*4],trans['output'][3+i*4])
 
        # Time step control.
        indexes = [ i for i, string in enumerate(min3pDatLines) if string.startswith("'time step control - global system'") ]
        min3pDatLines[indexes[0] + 3] = '%f               ; final solution time \n'%trans['finaltime']
        min3pDatLines[indexes[0] + 4] = '%f               ; maximum time step \n'%trans['maxtimestep']
        min3pDatLines[indexes[0] + 5] = '%f               ; minimum time step \n'%trans['mintimestep']
        
        # Topside boundary condition concentration
        indexes = [ i for i, string in enumerate(min3pDatLines) if string.startswith("'boundary conditions - reactive transport'") ]
        min3pDatLines[indexes[0] + 11] = "%f      'free'      'cl-1' \n"%trans['concentration']
        min3pDatLines[indexes[0] + 12] = "%f      'free'      'na+1' \n"%trans['concentration']
        
    else:
        # Transport Deactivation
        indexes = [ i for i, string in enumerate(min3pDatLines) if string.startswith("'global control parameters'") ]
        min3pDatLines[indexes[0] + 5] = '.false.              ;reactive_transport\n'
     
# Write new lines to .dat-file     
with open(path['filesDir']+'min3p.dat', 'w') as f:
    f.write(''.join(min3pDatLines))

# Create the ibnd-file.    
ibnd_creator.ibnd_creator(path['filesDir'])
if not homogeneous:
    # Create the hyc-file.
    hyc_creator.hyc_creator(path['filesDir'], path['templateDir'])

# Counter for the number of cases.
c = 0
# Create a case for each different GW-Influx value.
for i in influx:
    # Open the parameters dictionary file.
    with open('input.pickle0', 'r') as f:
        parameters, pressure = cPickle.load(f)
    # Add the influx for the case.
    parameters['influx']=i    
    # Save the parameters dictionary.
    with open('input.pickle0', 'w') as f:
        cPickle.dump([parameters, pressure], f)

    # Create case directory.
    path['currentJobDir'] = path['destDir'] + str(c)
    os.mkdir(path['currentJobDir']) 
    
    # Add boundary conditions to dat-file and save to currentJobDir.
    pressure_creator.pressure_creator(path['filesDir'], path['currentJobDir'])
    
    # Copy all created files to the case directory.
    shutil.copyfile('min3p.ibnd',path['currentJobDir']+'/min3p.ibnd')
    shutil.copyfile('input.pickle0',path['currentJobDir']+'/input.pickle0')
    if not homogeneous: 
        shutil.copyfile('min3p.hyc',path['currentJobDir']+'/min3p.hyc')
 
    # Open submission script
    with open(path['templateDir']+'FLI.sub', 'r') as f:
        subLines = f.readlines()
    # ... modify it ...
    for dirKey in ['currentJobDir','streamtracers_file']:
        subLines[[ i for i, string in enumerate(subLines) if string.startswith(dirKey)][0]]=dirKey+"='"+path[dirKey]+"'\n"
    subLines[[ i for i, string in enumerate(subLines) if string.startswith('#$ -o ')][0]]='#$ -o '+path['currentJobDir']+'/FLI.out\n'
    # ... write it to current folder.
    with open( path['currentJobDir']+'/FLI.sub', 'w') as f:
        f.write(''.join(subLines))
    
    # Increase case counter
    c=c+1
    # Submit job.
    os.system('qsub '+ path['currentJobDir'] +'/FLI.sub')
