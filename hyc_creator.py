# -*- coding: utf-8 -*-

def hyc_creator(filesDir, templatesDir):

    import cPickle
    
    with open(filesDir+'input.pickle0', 'r') as f:
        parameters, pressure = cPickle.load(f)
            
    
    with open(templatesDir+'randomField_esri.asc','r') as f:
        lines = f.readlines()
        l = list()
        for i in range(6,26):
            l.append(list())  
            for value in lines[i].split():
                l[i-6].append(value)
        # l contains all lines as lists beginning with the upper most line
        # 0,1 upper most 1 layer
        # 2, .. , 18 heterogeneous layer            
        # 19 bot layer        
        f.close()
    '''   
    Min3p hyc-file structure 
    
    title = "dataset min3p.hyc"
    variables = "x", "y", "z", "K_xx", "K_yy", "K_zz"
    zone t = "K - initial [m/s]",i = 201, j = 1, k = 51, f=point
    x y z kxx kyy kzz
    '''
    # Number of cells in bottom and mid layer combined
    nr_cells_bot_mid = parameters['nr_cells_mid'] + parameters['nr_cells_bot']
    # Number of cells in bottom, mid and inter layer combined
    nr_cells_bot_mid_inter = nr_cells_bot_mid + parameters['nr_cells_inter']
    # Number of cells in x-direction
    nr_cells_x = int(parameters['sedimentlength'] / parameters['cellx'])

    
    # How many cells in one heterogeneity field
    het_x = int ( 0.01 / parameters['cellx'] )  
    het_z = int ( 0.01 / parameters['hetcellz'] ) 
    
    # End of the area described in 'randomField_esri.asc'
    endofknownzone = 525 * het_x
    
    with open(filesDir+'min3p.hyc','w') as o:
        # write file header
        o.write('title = "dataset min3p.hyc"' + '\n')
        o.write('variables = "x", "y", "z", "K_xx", "K_yy", "K_zz"' + '\n')
        o.write('zone t = "K - initial [m/s]",i = %d, j = %d, k = %d, f=point'%(
                                                                            nr_cells_x,
                                                                            parameters['nr_cells_y'] ,
                                                                            parameters['nr_cells_top'] + parameters['nr_cells_inter']+ parameters['nr_cells_mid'] + parameters['nr_cells_bot']) + '\n')
        
        # write K values
        # from bottom to top
        for j in range(parameters['nr_cells_bot']+parameters['nr_cells_mid']+parameters['nr_cells_inter']+parameters['nr_cells_top']):   
            # from front to rear
            for y in range(parameters['nr_cells_y']):    
                # left to right
                for i in range(nr_cells_x):
                    # write always like this:  x, y, z, k, k, k 
                    # reminder: k0 == coarse sand, k1 == middle sand, k2 == fine sand
                    # bot layer cells
                    if j < parameters['nr_cells_bot'] : 
                        k = parameters['k0']         
                        o.write('%1.4e %1.4e %1.4e %1.4e %1.4e %1.4e'%(
                            parameters['cellx'] * i,
                            parameters['celly'] * y,
                            parameters['botcellz'] * j,
                            k,
                            k,
                            k,) + '\n')
                    # heterogeneous layer cells        
                    elif j < nr_cells_bot_mid:
                        # inside of the zone described in 'randomField_esri.asc' use the information from the file
                        if i < endofknownzone:                
                            k = int( l[(18-int((j-parameters['nr_cells_bot'])/het_z))][int(i/het_x)] )
                            if k == 1: 
                                k = parameters['k1']
                            elif k == 0: 
                                k = parameters['k0']
                            elif k == 2:
                                k = parameters['k2']
                        # outside set k = k2
                        else:
                            k = parameters['k2']
                        o.write('%1.4e %1.4e %1.4e %1.4e %1.4e %1.4e'%(
                            parameters['cellx'] * i,
                            parameters['celly'] * y,
                            parameters['botcellz'] * parameters['nr_cells_bot'] + parameters['hetcellz'] * (j-2),
                            k,
                            k,
                            k,) + '\n')  
                    # inter layer cells, always k1
                    elif j < ( parameters['nr_cells_mid'] + parameters['nr_cells_bot'] + parameters['nr_cells_inter'] ):
                        k = parameters['k1']
                        o.write('%1.4e %1.4e %1.4e %1.4e %1.4e %1.4e'%(
                            parameters['cellx'] * i,
                            parameters['celly'] * y,
                            parameters['botcellz'] * parameters['nr_cells_bot'] + parameters['hetcellz'] * parameters['nr_cells_mid'] + (j - nr_cells_bot_mid) * parameters['cellzi'],
                            k,
                            k,
                            k,) + '\n')   
                    # ripple cells, always k1
                    elif j >= ( parameters['nr_cells_mid'] + parameters['nr_cells_bot'] + parameters['nr_cells_inter'] ):
                        k = parameters['k1']
                        o.write('%1.4e %1.4e %1.4e %1.4e %1.4e %1.4e'%(
                            parameters['cellx'] * i,
                            parameters['celly'] * y,
                            parameters['botcellz'] * parameters['nr_cells_bot'] + parameters['hetcellz'] * parameters['nr_cells_mid'] + parameters['nr_cells_inter'] * parameters['cellzi'] + (j - nr_cells_bot_mid_inter) * parameters['rippleheight']/parameters['nr_cells_top'] ,
                            k,
                            k,
                            k,) + '\n')  

if __name__ == '__main__':
    hyc_creator()      