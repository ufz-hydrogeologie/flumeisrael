# -*- coding: utf-8 -*-
try: paraview.simple
except: from paraview.simple import *
import cPickle
import os
import sys
import getpass
import glob
# prepare python environment in order to load numpy and scipy
if (os.name == 'nt'):
    if (getpass.getuser() == 'laubeg'):
        os.chdir('C:\\Users\\laubeg\\Desktop\\0')
        sys.path.append('C:\\Program Files\\Anaconda\\Lib\\site-packages')
        if 'secondRun' in globals().keys():
            Delete(tecplotReader1)
            Delete(calculator1)
            Delete(mergeBlock1)
            Delete(programmableFilter1)
            Delete(streamTracer1)
            Delete(invalidSelectionSource1)
            Delete(invalidSelection)
            Delete(validSelectionSource1)
            Delete(validSelection)
        else :
            secondRun = True
    elif (getpass.getuser() == 'knabed'):
        os.chdir('C:\\Users\\knabed\\Desktop\\normal\\')
        sys.path.append('C:\\Python27\\Lib\\site-packages')
        if 'secondRun' in globals().keys():
            Delete(TecplotReader1)
            Delete(Calculator1)
            Delete(MergeBlock1)
            Delete(ProgrammableFilter1)
            Delete(StreamTracer1)
            Delete(InvalidSelectionSource1)
            Delete(InvalidSelection)
            Delete(ValidSelectionSource1)
            Delete(ValidSelection)
            Delete(TecplotReader2)
            Delete(Transform1)
            Delete(Transform2)
            
elif (os.name == 'posix'):
    if (getpass.getuser() == 'laubeg'):
        # os.chdir('/work/laubeg/transport_flumeIsrael/test/0')
        sys.path.append('/home/laubeg/local/python_2.7.6/lib/python2.7/site-packages')
    elif (getpass.getuser() == 'knabed'):
        #os.chdir('/work/knabed/transport_flumeIsrael/test/')
        sys.path.append('/home/knabed/local/python_2.7.6/lib/python2.7/site-packages')
        
import numpy as np
from vtk.util.numpy_support import vtk_to_numpy
import scipy

with open('input.pickle0', 'r') as f:
    parameters, pressure = cPickle.load(f)

# create a new 'Tecplot Reader'
tecplotReader1 = TecplotReader(FileNames=['./min3p_1.vel'])
tecplotReader1.DataArrayStatus = ['vx', 'vy', 'vz']

# create a new 'Calculator'
calculator1 = Calculator(Input=tecplotReader1)
calculator1.Function = 'vx * iHat + vy * jHat + vz * kHat'

# mergeBlocks as workaround to get correct vtk data format
mergeBlock1 = MergeBlocks()
# Get upper layer as point source
programmableFilter1 = ProgrammableFilter()
programmableFilter1.RequestUpdateExtentScript = ''
programmableFilter1.PythonPath = ''
programmableFilter1.RequestInformationScript = ''
programmableFilter1.Script = "import numpy as np\n\
from vtk.util.numpy_support import vtk_to_numpy\n\
pdi = self.GetInput()\n\
pdo = self.GetOutput()\n\
points = vtk_to_numpy(pdi.GetPoints().GetData())\n\
grid = np.reshape(points, (len(np.unique(points[:,0])), len(np.unique(points[:,1])), -1, 3), order='F')\n\
upperLayer = grid[:,[1],-1,:]\n\
upperLayerList = np.reshape(upperLayer, (-1,3), order='F')\n\
myPts = vtk.vtkPoints()\n\
for i in range(len(upperLayerList)):\n\
    myPts.InsertPoint(i, upperLayerList[i,0], upperLayerList[i,1], upperLayerList[i,2])\n\
pdo.SetPoints(myPts)\n\
"

mergeBlockData = servermanager.Fetch(mergeBlock1)
points = vtk_to_numpy(mergeBlockData.GetPoints().GetData())

grid = np.reshape(
    points, 
    (len(np.unique(points[:,0])), len(np.unique(points[:,1])), -1, 3), 
    order='F'
)
upperLayer = grid[:,[1],-1,:]
upperLayerList = np.reshape(upperLayer, (-1,3), order='F')
secondLowestZ = grid[0,0,1,2]

# create a new 'Stream Tracer With Custom Source'
streamTracer1 = StreamTracerWithCustomSource(Input=calculator1,
    SeedSource=programmableFilter1)
streamTracer1.Vectors = ['POINTS', 'Result']
streamTracer1.MaximumStreamlineLength = 100.
streamTracer1.TerminalSpeed = 1e-12
streamTracer1.MaximumError = 1e-06
if (parameters['influx'] <= 0.): # losing/neutral
    streamTracer1.IntegrationDirection = 'FORWARD'
else: # gaining
    streamTracer1.IntegrationDirection = 'BACKWARD'

###################################################################################################
# get data from proxy
streamTracerData = servermanager.Fetch(streamTracer1)

stPoints = vtk_to_numpy(streamTracerData.GetPoints().GetData())
vx = vtk_to_numpy(streamTracerData.GetPointData().GetArray('vx'))
vy = vtk_to_numpy(streamTracerData.GetPointData().GetArray('vy'))
vz = vtk_to_numpy(streamTracerData.GetPointData().GetArray('vz'))
integrationTime = vtk_to_numpy(streamTracerData.GetPointData().GetArray('IntegrationTime'))

# Extract point Ids in "cell shaped" list, i.e. pointIds[0] contains all pointIds of the first 
# cell, where pointIds[0][0]is the seed and pointIds[0][-1] is the exfiltration point. ParaView
# automaticly lists only those streamTraces (=cells), that survive at least one time step.
cellNum = streamTracerData.GetNumberOfCells()
pointNum = streamTracerData.GetNumberOfPoints()
pointIds = []
for cell in range(cellNum):
    pointIds.append([streamTracerData.GetCell(cell).GetPointIds().GetId(id) 
        for id in range(streamTracerData.GetCell(cell).GetPointIds().GetNumberOfIds())])
# for losing and neutral, stStartIds are sources, for gaining, stStartIds are sinks
stStartIds = np.array([pointIds[point][0] for point in range(len(pointIds))])
stEndIds = np.array([pointIds[point][-1] for point in range(len(pointIds))])

rippleZMin = np.min(upperLayerList[:,2])
if (parameters['influx'] <= 0.): # losing/neutral
    reInfiltratingCellIds = np.where((vz[stEndIds] > 0) | (stPoints[stEndIds,2] > rippleZMin))[0]
else: # gaining
    reInfiltratingCellIds = np.where((vz[stEndIds] < 0) | (stPoints[stEndIds,2] > rippleZMin))[0]

unusedSeedIds = np.setdiff1d(range(pointNum), np.hstack(pointIds))
seedIds = np.union1d(unusedSeedIds, stStartIds)
# SeedIdsStStartIds points to where the stStartIds are located in the seedIds-array
# e.g. vx[seedIds[SeedIdsStStartIds[1]]] == vx[stStartIds[1]]
# a lot of the first seeds were unused(because they did not survive one timestep), thus the second
# used seed point is SeedIdsStStartIds[1] == 17. seedIds[17] == stStartIds[1] == 18, because vx[18]
# is the starting-velocity corresponding to the second used stream-tracer
SeedIdsStStartIds = np.where(np.in1d(seedIds, stStartIds))[0]

# Calculate flux normal to Area ###################################################################
# dz/dx at point m,n is (z_m,n+1 - z_m,n-1)/ (x_m,n+1 - x_m,n-1)
# dz/dy at point m,n is (z_m+1,n - z_m-1,n)/ (x_m+1,n - x_m-1,n)
dx = (
    np.vstack((upperLayer[1:, :, 0], upperLayer[[-1], :, 0])) - 
    np.vstack((upperLayer[[0], :, 0], upperLayer[:-1, :, 0]))
)
dy = (
    np.hstack((upperLayer[:, 1:, 1], upperLayer[:, [-1], 1])) - 
    np.hstack((upperLayer[:, [0], 1], upperLayer[:, :-1, 1]))
)
dz_x = (
    np.vstack((upperLayer[1:, :, 2], upperLayer[[-1], :, 2])) - 
    np.vstack((upperLayer[[0], :, 2], upperLayer[:-1, :, 2]))
)
dz_y = (
    np.hstack((upperLayer[:, 1:, 2], upperLayer[:, [-1], 2])) - 
    np.hstack((upperLayer[:, [0], 2], upperLayer[:, :-1, 2]))
)
dz_dx = dz_x / dx
# calculate dz_dy and dy only if 2D upperLayer, dz_dy = 0 and dy = 2*parameters['sizey'] otherwise
# 2*sizey because it will be divided by two in area calculation
if np.shape(upperLayer)[1] > 1:
    dz_dy = dz_y / dy
else:
    dy = np.ones(np.shape(dy), np.float) * parameters['sizey'] * 2
    dz_dy = np.zeros(np.shape(upperLayer[:,:,0]), np.float)
# Normal Vector = [dz_dx, dz_dy, -1]
N = np.dstack((dz_dx, dz_dy, -np.ones(np.shape(upperLayer[:,:,0]))))
unit_N = np.divide(N, np.linalg.norm(N,axis=2)[:,:, np.newaxis])
velocity = np.dstack((
    np.reshape(vx[seedIds], np.shape(N[:,:,0]), order='F'),
    np.reshape(vy[seedIds], np.shape(N[:,:,0]), order='F'),
    np.reshape(vz[seedIds], np.shape(N[:,:,0]), order='F'))
)
proj_vel = (
    velocity[:, :, 0] * unit_N[:, :, 0] + 
    velocity[:, :, 1] * unit_N[:, :, 1] + 
    velocity[:, :, 2] * unit_N[:, :, 2])

# resulting velocity, don't use for q!
res_vel = np.linalg.norm(velocity,axis=2)

# area = area of rectangle with sidelength dx(in 2D)/2 * dy(in 2D)/2
area = np.linalg.norm([dx, dz_x],axis=0) * np.linalg.norm([dy, dz_y],axis=0) / 4.
# projected area, don't use for q!
area2 = dx * dy / 4.

# volumetric flow per seed
q = area * proj_vel

# qStartList contains volumetric flow of each used seed point.
qStartList = np.reshape(q, np.shape(seedIds), order='F')[SeedIdsStStartIds]
# hyporheicQ and -RT are lists of q and RT of returning (=hyporheic) stream traces
hyporheicQ = np.abs(qStartList[reInfiltratingCellIds])
hyporheicRT = np.abs(integrationTime[stEndIds][reInfiltratingCellIds])

# Create Streamtracer Selections ##################################################################
# The list of IDs that will be added to the selection produced by the selection source. This takes 
# pairs of values as (process number, id). (leave process = 0)
invalidCellIds = list(set(range(cellNum))-set(reInfiltratingCellIds))
ids = [0] * (len(invalidCellIds) * 2)
ids[1::2] = invalidCellIds
invalidSelectionSource1 = IDSelectionSource()
invalidSelectionSource1.IDs = ids
SetActiveSource(streamTracer1)
invalidSelection = ExtractSelection()
invalidSelection.Selection = invalidSelectionSource1
invalidSelectionData = servermanager.Fetch(invalidSelection)
invalidPoints = vtk_to_numpy(invalidSelectionData.GetPoints().GetData())

# same for valid cells
ids = [0] * (len(reInfiltratingCellIds) * 2)
ids[1::2] = reInfiltratingCellIds
validSelectionSource1 = IDSelectionSource()
validSelectionSource1.IDs = ids
SetActiveSource(streamTracer1)
validSelection = ExtractSelection()
validSelection.Selection = validSelectionSource1
validSelectionData = servermanager.Fetch(validSelection)
validPoints = vtk_to_numpy(validSelectionData.GetPoints().GetData())
hyporheicPoints = np.concatenate((validPoints, upperLayerList), axis=0)

# save raw data, in case calculateHyporheicArea() wont work, also save Q and RT
with open('./streamTracer.pickle2', 'wb') as f:
	cPickle.dump((validPoints, invalidPoints, grid, hyporheicQ, hyporheicRT), f, protocol=2)
    
def calculateHyporheicArea(validPoints, invalidPoints, grid) :
    # Create interpolator #########################################################################
    from scipy.interpolate import NearestNDInterpolator
    # hyporheic = 1, nonHyporheic = 0, stream = -1, interface = hyporheic
    hyporheicPoints = np.concatenate((validPoints, np.ones((len(validPoints), 1))), axis=1)
    nonHyporheicPoints =  np.concatenate((invalidPoints, np.zeros((len(invalidPoints), 1))), axis=1)
    interfaceList = np.reshape(grid[:,:,-1,:], (-1,3), order='F')
    interfacePoints = np.concatenate((interfaceList, np.ones((len(interfaceList), 1))), axis=1)
    # stream is 0.0001m above interface
    streamPoints = interfaceList + [0,0,0.0001]
    streamPoints = np.concatenate((streamPoints, -np.ones((len(streamPoints), 1))), axis=1)
    combinedPoints = np.concatenate((hyporheicPoints, nonHyporheicPoints, streamPoints, interfacePoints))
    ip_nearest = NearestNDInterpolator((combinedPoints[:, 0:3]), combinedPoints[:, 3])
    
    # Create regular grid #########################################################################
    regGridX = np.arange(np.min(grid[:,:,:,0]), np.max(grid[:,:,:,0]), 0.005)
    # 2D only, for now. Y = Y[1]
    regGridY = np.mean(grid[:,1,:,1])
    regGridZ = np.arange(np.min(grid[:,:,:,2]), np.max(grid[:,:,:,2]), 0.005)
    regGridX, regGridY, regGridZ = np.meshgrid(regGridX, regGridY, regGridZ)
    gridList = np.array([np.reshape(regGridX, (-1)), 
                         np.reshape(regGridY, (-1)), 
                         np.reshape(regGridZ, (-1))]).T
    
    # interpolate and calculate hyporheic area fraction ###########################################
    interpolated = ip_nearest(gridList)
    regGridType = np.reshape(interpolated, np.shape(regGridX))
    # remove stream entries 
    interpolated = interpolated[np.where(interpolated != -1)]
    hypFrac = np.mean(interpolated)
    with open('./hyporheicArea.pickle2', 'wb') as f:
        cPickle.dump((hypFrac, regGridX, regGridY, regGridZ, regGridType), f, protocol=2)
    return hypFrac
try: print('Hyporheic Area = %.2f%%' %(100*calculateHyporheicArea(validPoints, invalidPoints, grid)))
except: print('Scipy does not work with paraview on this machine!')


####################################################
# Plot the concentration pictures with streamtracers.

#### disable automatic camera reset on 'Show'
paraview.simple._DisableFirstRenderCameraReset()

# Load the min3p concentration output files ('.gsc') and sort them correctly.
# Sorting necessary to get the correct order of the gsc files for later use. 
sorted_gscFiles = sorted(glob.glob('./*.gsc'), key = lambda item : float(item.split('_')[1].split('.')[0]))

gsc = TecplotReader(FileNames=sorted_gscFiles)
gsc.DataArrayStatus = ['cl-1', 'na+1']

# get active view
renderView1 = GetActiveViewOrCreate('RenderView')


# show Concentration data in view
gsc_Display = Show(gsc, renderView1)
# trace defaults for the display properties.
gsc_Display.Representation = 'Surface'
gsc_Display.ColorArrayName = [None, '']
#gsc_Display.ScalarOpacityUnitDistance = 0.0967361324334136

# reset view to fit data
renderView1.ResetCamera()

# set scalar coloring
ColorBy(gsc_Display, ('POINTS', 'cl-1'))

# rescale color and/or opacity maps used to include current data range
gsc_Display.RescaleTransferFunctionToDataRange(True)

sbar = servermanager.rendering.ScalarBarWidgetRepresentation()
sbar.Title = 'Cl [mol/l]'
dataRepresentation1 = Show(gsc)
sbar.LookupTable = dataRepresentation1.LookupTable
sbar.Position = [0.25, 0.2]
sbar.Position2 = [0.4999999999999998, 0.13]
sbar.TitleFontSize=12
sbar.LabelFontSize=12
sbar.Orientation='Horizontal'
renderView1.Representations.append(sbar)

# get color transfer function/color map for 'cl1'
cl1LUT = GetColorTransferFunction('cl1')
cl1LUT.RGBPoints = [9.999999974752427e-07, 0.231373, 0.298039, 0.752941, 0.49999944121066225,
                    0.865003, 0.865003, 0.865003, 1.0, 0.705882, 0.0156863, 0.14902]
cl1LUT.LockScalarRange = 1
cl1LUT.ScalarRangeInitialized = 1.0

# get opacity transfer function/opacity map for 'cl1'
cl1PWF = GetOpacityTransferFunction('cl1')
cl1PWF.Points = [9.999999974752427e-07, 0.0, 0.5, 0.0, 1.0, 1.0, 0.5, 0.0]
cl1PWF.ScalarRangeInitialized = 1
# Rescale transfer function
cl1LUT.RescaleTransferFunction(0.0, 1.0)

# Rescale transfer function
cl1PWF.RescaleTransferFunction(0.0, 1.0)

# reset view to fit data
renderView1.ResetCamera()

# reset view to fit data
renderView1.ResetCamera()
# set background colour (RGB), comment for standard gray
renderView1.Background = [0,0,0]

# create a new 'Slice'
slice1 = Slice(Input=gsc)
slice1.SliceType = 'Plane'
slice1.SliceOffsetValues = [0.0]
# init the 'Plane' selected for 'SliceType'
slice1.SliceType.Origin = [2.7200000286102295, 0.055, 0.11805000016465783]
# Properties modified on slice1.SliceType
slice1.SliceType.Normal = [0.0, 1.0, 0.0]

# create a new 'Contour'
contour1 = Contour(Input=slice1)
contour1.ContourBy = ['POINTS', 'cl-1']
contour1.Isosurfaces = [0.8]
contour1.PointMergeMethod = 'Uniform Binning'

# show data in view
invalidDisplay = Show(invalidSelection, renderView1)
validDisplay = Show(validSelection, renderView1)
contourDisplay = Show(contour1, renderView1)
# change solid color
invalidDisplay.DiffuseColor = [1.0, 0.0, 0.0]
validDisplay.DiffuseColor = [0.0, 1.0, 0.0]
contourDisplay.DiffuseColor = [0.0, 0.0, 1.0]

# show data in view
slice1Display = Show(slice1, renderView1)
# trace defaults for the display properties.
slice1Display.ColorArrayName = ['POINTS', 'cl-1']
slice1Display.LookupTable = cl1LUT
Hide(gsc, renderView1)

# Only necessary when using the GUI, hides the transformation boxes.
Hide3DWidgets(proxy=slice1)

# current camera placement for renderView1
renderView1.CameraPosition = [2.953926594049286, -0.43, 0.124286324876562]
renderView1.CameraFocalPoint = [2.953926594049286, 0.055, 0.124286324876562]
renderView1.CameraViewUp = [0.0, 0.0, 1.0]
renderView1.CameraParallelScale = 2.7230125243620518

# Set output resolution ( if using the GUI the output resolution must not exceed 
# the actual display resolution otherwise it will only display the background)
renderView1.ViewSize = [ 2500,500]
renderView1.OrientationAxesVisibility = 0

# get animation scene
animationScene1 = GetAnimationScene()
# counter
c = 0
for i in sorted_gscFiles:
    # open gsc-file
    with open(i,'r') as f:
        # read all lines
        lines = f.readlines()
        # read the time
        # for all gsc files which are not the intial file min3p_0.gsc
        try: time = int(float(lines[2].split()[7]))
        # for the initial file set time to 0
        except : time = 0
        # go the the next animation scene
        animationScene1.AnimationTime = c
        # write image
        Hide(contour1, renderView1)
        WriteImage('tracer_'+str(time)+'_s.png')
        Hide(slice1, renderView1)
        Hide(validSelection, renderView1)
        Hide(invalidSelection, renderView1)
        if (os.name == 'posix'): ExportView('contour_'+str(time)+'_s.svg') #, Rasterize3Dgeometry=0) (works with pv4.4)
        elif (os.name == 'nt'): WriteImage('contour_'+str(time)+'_s.png')
        Hide(contour1, renderView1)
        Show(validSelection, renderView1)
        if (os.name == 'posix'): ExportView('validSelection_'+str(time)+'_s.svg') #, Rasterize3Dgeometry=0)
        elif (os.name == 'nt'): WriteImage('validSelection_'+str(time)+'_s.png')
        Hide(validSelection, renderView1)
        Show(invalidSelection, renderView1)
        if (os.name == 'posix'): ExportView('invalidSelection_'+str(time)+'_s.svg') #, Rasterize3Dgeometry=0)
        elif (os.name == 'nt'): WriteImage('invalidSelection_'+str(time)+'_s.png')
        Show(validSelection, renderView1)
        Show(contour1, renderView1)
        Show(slice1, renderView1)
    c = c + 1