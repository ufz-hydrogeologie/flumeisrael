# -*- coding: utf-8 -*-

def ibnd_creator(workingDir):
    import cPickle
    
    with open(workingDir+'input.pickle0', 'r') as f:
        parameters, pressure = cPickle.load(f)
    
    
    '''
    ibnd structure
    
    
    title = "dataset deformed mesh"
    variables = "x", "y", "z", "ibnd_vs", "ibnd_rt"
    zone t = "vertically deformed mesh",i = nr_cells_in_x , j = nr_cells_in_y , k = nr_cells_in_z, f=point
    
    x y z pressure_boundary_condition_number chemical_boundary_condition_number  
    '''
    # Number of cells for mid and bot combinded
    nr_cells_bot_mid = parameters['nr_cells_mid'] + parameters['nr_cells_bot']
    # Number of cells for mid, bot and inter combinded
    nr_cells_bot_mid_inter = parameters['nr_cells_mid'] + parameters['nr_cells_bot'] + parameters['nr_cells_inter']
    # Number of cells in x-direction
    nr_cells_x = int(parameters['sedimentlength'] / parameters['cellx'])
    # Number of cells for a ripple in x-direction
    nr_cells_rippel = int((parameters['ripplefall'] + parameters['ripplerise']) / parameters['cellx'])
    # Number of cells for the rise of a ripple in x-direction
    nr_cells_rise = int(parameters['ripplerise'] / parameters['cellx'])
    # Number of cells for the fall of a ripple in x-direction
    nr_cells_fall = int(parameters['ripplefall'] / parameters['cellx'])
    # Size of the cells in z-direction for the top 
    topcellz = parameters['rippleheight'] / (parameters['nr_cells_top'])
    # Height of the bottom and mid layer combined
    bot_mid_height = parameters['nr_cells_bot']*parameters['botcellz']+parameters['nr_cells_mid']*parameters['hetcellz'] 
    # Height of the bottom, mid and inter layer combined
    bot_mid_inter_height = bot_mid_height + parameters['nr_cells_inter']*parameters['cellzi']
    
    # size in z  heterogeneous 17 cm , bot course 2.5 cm
    # size in y  0.145 m  
    
    # Counter for the boundary conditions
    c=2
    

    with open(workingDir+'min3p.ibnd', "w") as f:
        # Write file header
        f.write('title = "dataset deformed mesh"'+'\n')
        f.write('variables = "x", "y", "z", "ibnd_vs", "ibnd_rt"'+'\n')
        f.write('zone t = "vertically deformed mesh",i = %d , j = %d , k = %d, f=point'%(
                                                                                    nr_cells_x,
                                                                                    parameters['nr_cells_y'],
                                                                                    parameters['nr_cells_bot'] + parameters['nr_cells_mid'] + parameters['nr_cells_top'] + parameters['nr_cells_inter'] + 1)+'\n\n')
        
        # from bottom to top
        for j in range(parameters['nr_cells_bot']+parameters['nr_cells_mid']+parameters['nr_cells_inter']+parameters['nr_cells_top']):
            # from front to rear
            for y in range(parameters['nr_cells_y']):
                # bot and mid layer cells
                if j<parameters['nr_cells_bot']+parameters['nr_cells_mid']:
                    # from left to right
                    for i in range(nr_cells_x):
                        # bot layer cells
                        # write the lowest cells with boundary condition 1
                        if j==0:
                            f.write('%f %f %f 1 0'%((i*parameters['cellx']) + parameters['cellx']/2,
                                                    y*parameters['celly']+parameters['celly']/2,
                                                    (j+1) * parameters['botcellz'])
                                                + '\n')
                        # wirte other bottom layer cells without boundary condition
                        elif j<parameters['nr_cells_bot']:
                            f.write('%f %f %f 0 0'%((i*parameters['cellx']) + parameters['cellx']/2,
                                                    y*parameters['celly']+parameters['celly']/2,
                                                    (j+1) * parameters['botcellz'])
                                                + '\n')
                        # mid layer cells
                        else:
                            f.write('%f %f %f 0 0'%((i*parameters['cellx']) + parameters['cellx']/2,
                                                    y*parameters['celly']+parameters['celly']/2,
                                                    (j-1) * parameters['hetcellz']+0.025)
                                                + '\n')
                # top layer cells
                else:
                    # from left to right
                    for i in range(nr_cells_x):
                        # inter layer cells
                        if j < parameters['nr_cells_bot']+parameters['nr_cells_mid']+parameters['nr_cells_inter']:
                            f.write('%f %f %f 0 0'%((i*parameters['cellx']) + parameters['cellx']/2,
                                                    y*parameters['celly']+parameters['celly']/2,
                                                    (j-nr_cells_bot_mid+1)*parameters['cellzi'] + bot_mid_height  )
                                                + '\n')
                           
                        # the ripple cells
                        # rise of a ripple
                        elif (i+parameters['xoffset'])%nr_cells_rippel < nr_cells_rise:
                            # if topmost layer write with correct boundary
                            if j == (parameters['nr_cells_bot']+parameters['nr_cells_mid']+parameters['nr_cells_inter']+parameters['nr_cells_top']-1):
                                f.write('%f %f %f %i 1'%((i*parameters['cellx']) + parameters['cellx']/2,
                                                        y*parameters['celly']+parameters['celly']/2,
                                                        (j-nr_cells_bot_mid_inter+1)*((i+parameters['xoffset'])%nr_cells_rippel+0.5) * topcellz/nr_cells_rise +bot_mid_inter_height,
                                                        c)
                                                    + '\n')
                                # increment boundarynumber counter
                                c=c+1
                            # if not topmost layer write no boundary 
                            else:
                                f.write('%f %f %f 0 0'%((i*parameters['cellx']) + parameters['cellx']/2,
                                                        y*parameters['celly']+parameters['celly']/2,
                                                        (j-nr_cells_bot_mid_inter+1)*((i+parameters['xoffset'])%nr_cells_rippel+0.5) * topcellz/nr_cells_rise +bot_mid_inter_height)
                                                    + '\n')
                        # decline of a ripple
                        else:
                            # if topmost layer write with correct boundary
                            if j == (parameters['nr_cells_bot']+parameters['nr_cells_mid']+parameters['nr_cells_inter']+parameters['nr_cells_top']-1):
                                f.write('%f %f %f %i 1'%((i*parameters['cellx']) + parameters['cellx']/2,
                                                        y*parameters['celly']+parameters['celly']/2,
                                                        (j-nr_cells_bot_mid_inter+1)*((nr_cells_rise-1)-(i+parameters['xoffset'])%nr_cells_rippel+0.5) * topcellz/nr_cells_fall +bot_mid_inter_height+(j-nr_cells_bot_mid_inter+1)*topcellz,
                                                        c )
                                                    + '\n')
                                # increment boundarynumber counter
                                c=c+1
                           # if not topmost layer write no boundary 
                            else:    
                                f.write('%f %f %f 0 0'%((i*parameters['cellx']) + parameters['cellx']/2,
                                                        y*parameters['celly']+parameters['celly']/2,
                                                        (j-nr_cells_bot_mid_inter+1)*((nr_cells_rise-1)-(i+parameters['xoffset'])%nr_cells_rippel+0.5) * topcellz/nr_cells_fall +bot_mid_inter_height+(j-nr_cells_bot_mid_inter+1)*topcellz )
                                                    + '\n')

        # top layer of cells which is not used in min3p but needs to be there
        for y in range(parameters['nr_cells_y']): 
            for i in range(nr_cells_x):

                # rise of a ripple
                if (i+parameters['xoffset'])%nr_cells_rippel < nr_cells_rise:
                    f.write('%f %f %f -1 0'%((i*parameters['cellx']) + parameters['cellx']/2,
                                            y*parameters['celly']+parameters['celly']/2,
                                            (parameters['nr_cells_top']+1)*((i+parameters['xoffset'])%nr_cells_rippel+0.5) * topcellz/nr_cells_rise +bot_mid_inter_height)
                                        + '\n')
                # decline of a ripple
                else:
                    f.write('%f %f %f -1 0'%((i*parameters['cellx']) + parameters['cellx']/2,
                                            y*parameters['celly']+parameters['celly']/2,
                                            (parameters['nr_cells_top']+1)*((nr_cells_rise-1)-(i+parameters['xoffset'])%nr_cells_rippel+0.5) * topcellz/nr_cells_fall +bot_mid_inter_height+(parameters['nr_cells_top']+1)*topcellz )
                                        + '\n')

                            

if __name__ == '__main__':
    ibnd_creator()                
            

