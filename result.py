# -*- coding: utf-8 -*-

import matplotlib
matplotlib.use('agg')
import matplotlib.pyplot as plt
import glob
import cPickle
import os
import numpy as np
import xlsxwriter

basedir = '/work/laubeg/flumeIsrael/'
# destfolders = [
    # 'fine_het_normal/',
    # 'fine_het_slow/',
    # 'fine_hom_iso_fit_normal/',
    # 'fine_hom_iso_fit_slow/',
    # 'fine_hom_aniso_normal/',
    # 'fine_hom_aniso_slow/',
    # 'fine_hom_iso_normal/',
    # 'fine_hom_iso_slow/',
    # 'fine_hom_aniso_fit_normal/',
    # 'fine_hom_aniso_fit_slow/']
destfolders = [
    'hom_1/',
    'hom_1_1/',
    'hom_1_2/',
    'hom_1_3/',
    'hom_1_4/',
    'hom_1_5/']


def weightedMedian(a, weights):
    sortingIds = np.argsort(a)
    a = a[sortingIds]
    weights = np.abs(weights[sortingIds])
    weightsCumSum = np.cumsum(weights)
    # returns lower median (side = 'left'), which might not be 100% accurate
    medianID = np.searchsorted(weightsCumSum, weightsCumSum[-1]/2.)
    return a[medianID]
    
workbook = xlsxwriter.Workbook(basedir+'results.xlsx')
    
for destfolder in destfolders:
    # List for the hyporheic flux of the cases
    hyporheicflux = list()
    hyporheicFraction = list()
    influxlist = list()
    lnRTMu = list()
    lnRTMedian = list()
    numST = list()

    for case in glob.glob( basedir+destfolder+'*'):
        if os.path.isdir(case) and not 'files' in case:
            with open(case + '/hyporheicArea.pickle2', 'r') as f:
                hypFrac, regGridX, regGridY, regGridZ, regGridType = cPickle.load(f)
            with open(case + '/streamTracer.pickle2', 'r') as f:
                validPoints, invalidPoints, grid, hyporheicQ, hyporheicRT = cPickle.load(f)
            with open(case + '/input.pickle0', 'r') as f:
                parameters, pressure = cPickle.load(f)
            with open(case + '/min3p_o.mvc','r') as f:
                Lines = f.readlines()
            print "-------------------------"
            print case
            # Get the total influx
            totalinflux = Lines[3].split()[1]
            # Print the GW influx
            # Remember: + == Inflow (gaining conditions), - == Outflow (losing conditions)
            print "GW_Influx[cm/d]: "+str(parameters['influx']*60*60*24*100)
            influxlist.append(parameters['influx'] * 60*60*24*100)
            # Print the total influx
            print "Total_Influx[cm/d]: "+str(float(totalinflux) / (5.44*0.145)*100) # convert m^3/d to cm/d 
            # Compute the hyporheic flux = total influx - abs ( GW-Influx )
            hyporheic = float(totalinflux) / (5.44*0.145)*100 - abs(parameters['influx']*60*60*24*100)  
            # Append the hyporheic flux value of the case to the list
            hyporheicflux.append(hyporheic)
            # print the hyporheic flux value of the case
            print "Total Hyporheic Flux[cm/d] :" +str(hyporheic)
            # calculate, plot and print hyporheic area (aka hyporheic fraction)
            sliceType = regGridType[0,:,:]
            fig = plt.figure(figsize=(20,2))
            plt.imshow(sliceType.T[::-1])
            fig.savefig(case + '/hyporheicArea.png', dpi=600)
            plt.close(fig)
            hyporheicFraction.append(hypFrac)
            print("Fraction of Hyporheic Area: %.2f%%" %(100*hypFrac))
            # plot weighted RT-histogram
            hist, bins = np.histogram(
                np.log(hyporheicRT), 
                weights=hyporheicQ,
                bins=int(np.sqrt(len(hyporheicRT)+1))
            )
            width = 0.7 * (bins[1] - bins[0])
            center = (bins[:-1] + bins[1:]) / 2
            fig = plt.figure()
            plt.bar(center, hist, align='center', width=width)
            fig.savefig(case + '/lnRThist.png')
            plt.close(fig)
            try:
                lnRTMu.append(np.average(np.log(hyporheicRT), weights=hyporheicQ))
                lnRTMedian.append(weightedMedian(np.log(hyporheicRT), weights=hyporheicQ))
            except:
                lnRTMu.append(0.0)
                lnRTMedian.append(0.0)
            print("Weighted Mean ln(Residence Time): %.2f" %(lnRTMu[-1]))
            print("Weighted Median ln(Residence Time): %.2f" %(lnRTMedian[-1]))
            numST.append(len(hyporheicRT))
            print("Number of Streamtracers used for calculation: %i" %(numST[-1]))
            
        
    # Flume Results

    realinflux = [0., -10.504, -39.5714, -59.2806, -97.9735, -146.0626, -197.0928, -254., -291.8196, 
                  10., 39.5714, 59.2806, 97.9735, 150., 190.9814, 245.6, 284., 
                  0., -10., -20., -29.55, -50., -50., -80., 
                  10., 20., 30., 49., 78.]
    realhypo = [89.6973, 84.5228, 77.1756, 66.3353, 50.8567, 36.5375, 26.1312, 27.0288, 27.0706, 
                84.9, 71.9523, 67.5784, 49.535, 37.1016, 27.4685, 26.6305, 26.8935, 
                21.8, 18.3745, 13.9982, 10.5479, 13.9862, 12.5559, 13.5958, 
                19.4394, 16.017, 10.9328, 8.1616, 9.4024]

    # Plot the Hyporheic Flux to GW-Influx

    fig = plt.figure()
    ax = plt.gca()
    # Plot points
    ax.plot(influxlist, hyporheicflux,'.')
    ax.plot(realinflux, realhypo,'.')
    # Set axis labels
    plt.xlabel('GW-Influx cm/day')
    plt.ylabel('Hyporheic Flux cm/day')
    # Set axis limits
    ax.set_xlim(-350,350)
    ax.set_ylim(0,150)
    # Save plot to file.
    fig.savefig(basedir + destfolder + 'result.png')
    plt.close(fig)

    fig = plt.figure()
    ax = plt.gca()
    # Plot points
    ax.plot(influxlist, np.array(hyporheicFraction)*100,'.')
    # Set axis labels
    plt.xlabel('GW-Influx cm/day')
    plt.ylabel('Volume-Fraction of Hyporheic Zone [%]')
    # Set axis limits
    ax.set_xlim(-350,350)
    ax.set_ylim(0,100)
    # Save plot to file.
    fig.savefig(basedir + destfolder + 'hypFrac.png')
    plt.close(fig)

    fig = plt.figure()
    ax = plt.gca()
    # Plot points
    ax.plot(influxlist, lnRTMedian,'.')
    ax.plot(influxlist, lnRTMu,'.')
    # Set axis labels
    plt.xlabel('GW-Influx cm/day')
    plt.ylabel('Median and Mean ln(Residence Time) [ln(s)]')
    # Set axis limits
    ax.set_xlim(-350,350)
    # Save plot to file.
    fig.savefig(basedir + destfolder + 'RTStats.png')
    plt.close(fig)

    resultArray = np.array([influxlist, hyporheicflux, hyporheicFraction, lnRTMu, lnRTMedian, numST]).T
    np.savetxt(
        basedir + destfolder + 'results.csv', 
        resultArray, 
        header='q_gw\tq_h\tA_h/A_tot\tlnRTMu\tlnRTMedian\tnumST',
        delimiter='\t'
    )
    worksheet = workbook.add_worksheet(destfolder.replace('/', ''))
    header = ['q_gw','q_h','A_h/A_tot','lnRTMu','lnRTMedian','numST']
    for col in range(len(header)):
        worksheet.write(0,col,header[col])
    for row in range(np.size(resultArray, 0)):
        for col in range(np.size(resultArray, 1)):
            worksheet.write_number(row+1, col, resultArray[row,col])


workbook.close()
