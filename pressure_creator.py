# -*- coding: utf-8 -*-
"""
Created on Thu Jul 16 13:41:20 2015

@author: knabed
"""
def pressure_creator(filesDir, currentDir):

    import numpy as np
    from scipy.interpolate import LinearNDInterpolator, NearestNDInterpolator
    import cPickle
        
    with open(filesDir+'input.pickle0', 'r') as f:
        parameters, pressure = cPickle.load(f)
    
    # If True, use the paraview export file(openfoam data) to interpolate heads. 
    if pressure['read']:
        if pressure['speed'] == 'normal':
            paraexport = 'ParaView_Export0_normal.csv'
        elif pressure['speed'] == 'slow':
            paraexport = 'ParaView_Export0_slow.csv'      
        xyzp = np.genfromtxt(paraexport, delimiter=',', names=True, 
                            usecols=('Points0', 'Points1', 'Points2', 'p_rghMean'))
                            # now uses hydraulic head, if pressure head is set in
                            # the dat file use 'pMean' instead of 'p_rghMean'
        # Linear Interpolator for points with multiple neighbours
        ip = LinearNDInterpolator((xyzp['Points0'],xyzp['Points1']), xyzp['p_rghMean'])
        # Nearest Neighbour Interpolator for points with only one neighbours / border points
        ip_nearest = NearestNDInterpolator((xyzp['Points0'],xyzp['Points1']), xyzp['p_rghMean'])
    # else use elliott & brooks 
    else:
        g = 9.81
        # calculate headVariation (Elliott & Brooks 1997)
        
        # because rippleheight / streamdeapth = 0.1 <=0.34
        headVariation=((pressure['empConstant']*(pressure['streamVelocity']**2.)/(2.*g)) * 
            ((parameters['rippleheight']/parameters['waterDepth']/0.34)**(3./8.)))
        waveNumber = pressure['periods'] * 2 * np.pi / parameters['sedimentlength']
        phaseShift = pressure['phaseshift'] * np.pi / 180.
        def elliotbrooks(x):
            # return head
            return (headVariation * np.sin(waveNumber * x + phaseShift) -  pressure['slope'] * x) + 0.22 + headVariation
            
    
    # Number of cells in x-direction
    nr_cells_x = int(parameters['sedimentlength'] / parameters['cellx'])
    
    
    # 0.096 (?3) is length of the ramp which is included in the paraview export values
    
    # Read the original dat file
    with open(filesDir+'min3p.dat', "r") as f:
        datFileLines = f.readlines()
    
    # Write the new dat file
    with open(currentDir+'min3p.dat', 'w') as f:
        # Write all lines from the old file until the boundary conditions water flow 
        for line in datFileLines:
            f.write(line)
            if "'boundary conditions - water flow'" in line:
                break   
        # Write the total number of boundary conditions
        f.write("%i        ;  'number of zones'"%(int(int(parameters['sedimentlength'] / parameters['cellx'])*parameters['nr_cells_y'])+1) + '\n')
        f.write("'read spatial boundary distribution from file'" + "\n")       
        # Write the botside GW-Flow boundary with the boundary condition number 1      
        f.write("'number and name of zone'" + '\n')
        f.write(str(1) + '\n')
        f.write("'Bot"+ str(1) + "'" + '\n')
        f.write("'boundary type'" + '\n')
        f.write("'second' " + str(parameters['influx']) + ' ;specified flux (m/s)\n') 
        f.write("'extent of zone'" + '\n')
        f.write('0 5.440 0 0.145 0.215 0.215'+ '\n') # should be unimportant boundary conditions are assigned to cells in ibnd-file
        f.write("'end of zone'" + '\n')
        
        # Counter to assign the numbers to the boundary conditions, these numbers are used in the 
        # ibnd-file to assign the boundary conditions to the correct cells.
        c = 2
        # Write all topside boundaries
        for y in range(parameters['nr_cells_y']):
            for i in range(nr_cells_x):
                # Write the boundary
                f.write("'number and name of zone'" + '\n')
                f.write(str(c) + '\n')
                f.write("'Top"+ str(c) + "'" + '\n')
                f.write("'boundary type'" + '\n')
                # If using the paraview export, use the interpolators, 
                # but their values have to converted to heads from Pa (multiplying with 0.0001019977334).  
                if pressure['read']:
                    # If the cell is an outer cell, meaning no linear interpolation possible, use the nearest neighbour interpolator
                    if str(ip((i*parameters['cellx']) + parameters['cellx']/2+ 0.96 + parameters['xoffset'] * parameters['cellx'] , y*parameters['celly'] ))== 'nan':
                        f.write("'first' " + str(0.0001019977334*ip_nearest((i*parameters['cellx']) + parameters['cellx']/2+ 0.96 + parameters['xoffset'] * parameters['cellx'], y*parameters['celly'] )) + '\n')
                    # else use the linear interpolator
                    else:            
                        f.write("'first' " + str(0.0001019977334*ip((i*parameters['cellx']) + parameters['cellx']/2+ 0.96 + parameters['xoffset'] * parameters['cellx'], y*parameters['celly'] )) + '\n')
                
                # If not using the paraview export (openfoam data), use the elliott & brooks boundary.
                else:
                    f.write("'first' " + str(elliotbrooks(i*parameters['cellx'] + parameters['cellx']/2)) + '\n')
                f.write("'extent of zone'" + '\n')
                f.write('0 5.440 0 0.145 0.215 0.215'+ '\n') # should be unimportant, because the boundary conditions are assigned to the cells via numbers.
                f.write("'end of zone'" + '\n')
                # Increment c after writing a boundary
                c=c+1
        f.write("'done'")

if __name__ == '__main__':
    pressure_creator()    