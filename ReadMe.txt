Einige Worte zum Skript:

Das meiste sollte auskommentiet sein, auch wenn nicht unbedingt immer ausf�hrlich.

Zu den Teilen:

Flume.py
 ist das main-file. Alle ver�nderlichen Parameter sind hier. Die wichtigsten
 Parameter stehen am Anfang. Wichtig ist ebenso, dass falls mal die Zellgr��en 
 ver�ndert werden sollen, die Zellgr��e und die Zellanzahl im Parameter-Dictionary 
 ver�ndert werden m�ssen. Es sollte funktionieren solange die Zellgr��en ganzahlige 
 Bruchteile von 0,005 sind. 

 
ibnd_creator , hyc_creator , pressure_creator 
 sind die extra ausgegliederten Teile, die das ibnd-file, das hyc-file und die Randbedingunen schreiben. 
 In der Regel werden, wenn h�ufig lange Summen von Parametern vorkommen, Variablen am Anfang festgelegt, 
 bspw. wenn die Zellanzahlen der verschiedenen Layer addiert werden um in den Schleifen festzulegen,
 wo gerade geschrieben wird. Manchmal kann das noch fehlen und es stehen stattdessen die langen Summen
 noch da, was dann etwas un�bersichtlich wird.
 Beim hyc und ibnd ist die Reihenfolge beim schreiben wichtig:
    for z ....
        for y ...
            for x ....
 Beim ibnd-file 
 gilt es zu beachten, dass nach Aussage von Nico die Mitte der Oberseite der Zellen angegeben wird und
 dass oberhalb der ganzen eigentlichen Domain zus�tzlich eine Reihe von Zellen angegeben wird, die sp�ter
 nicht verwendet werden sollen. Die Nummer der jeweiligen Randbedingung (wie sie so auch in das .dat-file
 vom pressure_creator geschrieben werden) werden jeder Zelle zugeordnet in den letzten beiden Spalten. Die zus�tzlichen Zellen
 sind gegekennzeichnet mit einer '-1' als Randbedingungsnummer, das ist nicht notwendig bietet sich aber
 an zur besseren �bersicht. 
 
para.py
 ist im streamtracers.py enthalten und damit obsolet.
 
result.py
 hier muss der Ausgabeordner in dem die einzelnen Cases drin sind wieder extra spezifiziert werden.
 Das Skript gibt alle GW-Influxes und Hyporheischen Fluxes aus f�r jeden Case und erstellt eine Diagramm mit
 Hyporheischer Austausch zu GW-Zustrom.
 
multiple_results.py
 mit dem Skript kann man mehrere Versuchsreihen vergleichen, man muss am Anfang in das Dictionary zu 
 jedem Namen als Key den Pfad als Wert eintragen, der rest sollte funktionieren, auch wenn das nicht
 explizit nochmal f�r andere Ergebnisse getesten worden ist.
 
streamtracers.py
 neben denn ganzen streamtracer berechnungen am Anfang, wird am ende noch die gsc-Files geladen und die
 Streamtracer �ber die gsc-Bilder gelegt. Dabei sind die hyporheischen Streamtracer gr�n und die nichthyporheischen rot.
 Aus dem gsc-Bild wird ein Slice genommen um eine 2D-Darstellung z u bekommen, da die Umstellung auf 2D-Darstellung des 
 3D-Bildes dazu f�hrt das die Kameraeinstellungen des Skriptes einfach ignoriert werden. Ebenso wichtig ist die ViewSize,
 wenn man die GUI verwendet darf die ViewSize resolution nicht gr��er sein als die Windows display resolution, sonst
 zeigt er nichts an.  
 